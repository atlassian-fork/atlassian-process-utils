package com.atlassian.utils.process;

import java.io.OutputStream;

/**
 * A Handler for process input
 *
 * The InputHandler interface is designed to allow different input provision strategies to be plugged into
 * process handlers
 */
public interface InputHandler {

    void complete();

    void process(OutputStream input);

    void setWatchdog(Watchdog watchdog);
}
