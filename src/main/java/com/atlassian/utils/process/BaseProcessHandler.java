package com.atlassian.utils.process;

/**
 */
public abstract class BaseProcessHandler implements ProcessHandler {

    private Watchdog watchdog;

    public void setWatchdog(Watchdog watchdog) {
        this.watchdog = watchdog;
    }

    public Watchdog getWatchdog() {
        return watchdog;
    }
}
