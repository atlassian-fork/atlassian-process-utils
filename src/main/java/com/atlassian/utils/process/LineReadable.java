package com.atlassian.utils.process;

import java.io.Closeable;
import java.io.IOException;

public interface LineReadable extends Closeable {

    String readLine() throws IOException;

}
