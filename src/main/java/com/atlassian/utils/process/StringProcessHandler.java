package com.atlassian.utils.process;

/**
 * A {@link ProcessHandler} implementation which collects process output into strings and, optionally,
 * provides input from a string.
 */
public class StringProcessHandler extends PluggableProcessHandler {
    private static final String DEFAULT_ENCODING = "UTF-8";

    private StringOutputHandler outputHandler;
    private StringOutputHandler errorHandler;

    public StringProcessHandler() {
        this(null, DEFAULT_ENCODING);
    }

    public StringProcessHandler(String input) {
        this(input, DEFAULT_ENCODING);
    }

    public StringProcessHandler(String input, String encoding) {
        if (input != null) {
            setInputHandler(new StringInputHandler(encoding, input));
        }

        outputHandler = new StringOutputHandler(encoding);
        setOutputHandler(outputHandler);

        errorHandler = new StringOutputHandler(encoding);
        setErrorHandler(errorHandler);
    }

    public String getOutput() {
        return outputHandler.getOutput();
    }

    public String getError() {
        return errorHandler.getOutput();
    }
}

