package com.atlassian.utils.process;

import javax.annotation.Nonnull;

/**
 * Defines a factory which accepts {@link ExternalProcessSettings} and constructs instances of some
 * {@link ExternalProcess} implementation based on them.
 */
public interface ExternalProcessFactory {

    /**
     * Creates an {@link ExternalProcess} configured by the provided settings.
     *
     * @param settings the settings to apply to the process
     * @return the created and configured process
     */
    @Nonnull
    ExternalProcess create(@Nonnull ExternalProcessSettings settings);

    /**
     * Shuts down the factory, indicating no more processes will be created.
     */
    void shutdown();
}
