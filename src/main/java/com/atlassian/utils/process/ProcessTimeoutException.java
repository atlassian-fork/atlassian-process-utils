package com.atlassian.utils.process;

/**
 *
 * This ProcessException is thrown when a process does not complete within the specified timeout period.
 *
 */
public class ProcessTimeoutException extends ProcessException {

    private static final long serialVersionUID = 5607536120405113374L;

    public ProcessTimeoutException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProcessTimeoutException(String message) {
        super(message);
    }

    public ProcessTimeoutException(Throwable e) {
        super(e);
    }

    public ProcessTimeoutException(String message, int exitCode) {
        super(message, exitCode);
    }
}
