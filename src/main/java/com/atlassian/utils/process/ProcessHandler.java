package com.atlassian.utils.process;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface used to manage the IO and termination needs of an external processes
 * managed by ExternalProcess.  The methods of this interface will be called by
 * different threads, so implementors should take care to ensure thread safety
 *
 * @see com.atlassian.utils.process.ExternalProcess
 */
public interface ProcessHandler {

    /**
     * Called when the external process has completed
     *
     * @param exitCode the exit code of the external process
     * @param canceled {@code true} to indicate the process was canceled, otherwise {@code false} if the process was
     *                 allowed to run to completion
     * @param exception any process exceptions that were thrown within the VM when handling the
     *        external process
     */
    void complete(int exitCode, boolean canceled, ProcessException exception);

    /**
     * Get any processing exception associated with this handler
     * @return a processing exception instance or null if no exception occurred.
     */
    ProcessException getException();

    /**
     * Get the process exit code
     * @return process exit code
     */
    int getExitCode();

    /**
     * Indicate if this handler has input to provide to the process
     *
     * @return true if input is available
     */
    boolean hasInput();

    /**
     * Retrieves a flag indicating whether the process was allowed to run to completion or was canceled.
     * <p>
     * Note: This flag only has meaning when {@link #isComplete()} returns {@code true}. If the process has not yet
     * completed, it is undefined whether or not the process has been canceled. Therefore, this method may return
     * {@code true} or {@code false} inconsistently for incomplete processes.
     *
     * @return {@code true} if the process was canceled; otherwise, {@code false} if it ran to completion
     */
    boolean isCanceled();

    /**
     * Indicate if the process has completed
     *
     * @return true if complete has been called.
     */
    boolean isComplete();

    /**
     * Process the process stderr stream
     *
     * @param error the external process' standard error stream (available as an input to this class)
     * @throws ProcessException if there is a problem processing the output
     */
    void processError(InputStream error) throws ProcessException;

    /**
     * Process the process stdout stream
     *
     * @param output the external process' output stream (available as an input to this class)
     * @throws ProcessException if there is a problem processing the output
     */
    void processOutput(InputStream output) throws ProcessException;

    /**
     * Provide input to the external process. Input is provided by writing the content to the
     * given output stream. This method will only be called if hasInput() returns true
     *
     * @param input the output stream representing standard input to the external process
     * @throws IllegalStateException if no input has been configured.
     */
    void provideInput(OutputStream input);

    /**
     * Called if the process is to be re-executed.
     */
    void reset();

    /**
     * Set the watchdog associated with this handler. The watchdog should be called at regular intervals
     * to prevent the external process being terminated. Typically this is done in the IO handling methods
     *
     * @param watchdog the watchdog for the process
     */
    void setWatchdog(Watchdog watchdog);

    /**
     * Indicate if the process execution has been considered successful.
     *
     * @return true if the process execution completed without error and was not cancelled
     */
    boolean succeeded();
}

