package com.atlassian.utils.process;

/**
 */
public class ProcessUtils {
    private ProcessUtils() {
    }

    public static String[] tokenizeCommand(String commandLine) {
        return commandLine.split("[ \\t\\n\\r\\f]+");
    }
}
