package com.atlassian.utils.process;

/**
 * {@link ProcessException} specialization thrown when an {@link ExternalProcess} cannot be
 * {@link ExternalProcess#start() started}.
 *
 * @since 1.5.10
 */
public class ProcessNotStartedException extends ProcessException {

    public ProcessNotStartedException(String message, Throwable cause) {
        super(message, cause);
    }
}
