package com.atlassian.utils.process;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Utility class to simplify the building of an {@link ExternalProcess} instance.
 */
public class ExternalProcessBuilder {

    private static ExternalProcessFactory externalProcessFactory = new DefaultExternalProcessFactory();

    private final ExternalProcessSettings settings = new ExternalProcessSettings();

    public static ExternalProcessFactory getExternalProcessFactory() {
        return externalProcessFactory;
    }

    public static void setExternalProcessFactory(ExternalProcessFactory factory) {
        if (factory == null) {
            throw new NullPointerException("factory");
        }
        externalProcessFactory = factory;
    }

    public ExternalProcessBuilder addMonitor(ProcessMonitor... monitors) {
        settings.getMonitors().addAll(Arrays.asList(monitors));

        return this;
    }

    /**
     * Hints to the built {@link ExternalProcess process} that it will be {@link ExternalProcess#start() started} and
     * run asynchronously rather than {@link ExternalProcess#execute() executed} synchronously.
     * <p>
     * Some aspects of how processes are wrapped up when they terminate are different when they are run asynchronously.
     * For example, {@link ProcessMonitor#onAfterFinished(ExternalProcess) onAfterFinished} may not be promptly invoked
     * for asynchronous processes. This hint allows the {@link ExternalProcess} implementation to attempt to adjust for
     * those differences.
     *
     * @return {@code this}
     */
    public ExternalProcessBuilder asynchronous() {
        settings.setAsynchronous(true);

        return this;
    }

    public ExternalProcess build() {
        return externalProcessFactory.create(settings);
    }

    public ExternalProcessBuilder command(List<String> command) {
        settings.setCommand(command);

        return this;
    }

    public ExternalProcessBuilder command(List<String> command, File workingDir) {
        settings.setWorkingDirectory(workingDir);

        return command(command);
    }

    public ExternalProcessBuilder command(List<String> command, File workingDir, long timeout) {
        idleTimeout(timeout);

        return command(command, workingDir);
    }

    public ExternalProcessBuilder env(String variable, String value) {
        settings.getEnvironment().put(variable, value);

        return this;
    }

    public ExternalProcessBuilder env(Map<String, String> environment) {
        settings.getEnvironment().putAll(environment);

        return this;
    }

    /**
     * Escape double quotes in arguments. Setting this has no effect if the process is not being run on Windows.
     *
     * @return {@code this}
     */
    public ExternalProcessBuilder escapeInternalDoubleQuotesOnWindows() {
        settings.setEscapeInternalDoubleQuotesOnWindows(true);

        return this;
    }

    /**
     * Sets the absolute number of milliseconds the process is allowed to execute <i>regardless of whether it is idle
     * or is actively producing output</i>. Setting this limit may be useful to prevent resource-intensive processes
     * from "running away", tying up the system while they execute for an indefinite amount of time. When this timeout
     * expires, the process will be killed even if it is not idle.
     *
     * @param executionTimeout the number of milliseconds the process is allowed to execute
     * @return {@code this}
     */
    public ExternalProcessBuilder executionTimeout(long executionTimeout) {
        settings.setExecutionTimeout(executionTimeout);

        return this;
    }

    public ExternalProcessBuilder handler(ProcessHandler handler) {
        settings.setProcessHandler(handler);

        return this;
    }

    public ExternalProcessBuilder handlers(OutputHandler output) {
        return handlers(null, output, null);
    }

    public ExternalProcessBuilder handlers(OutputHandler output, OutputHandler error) {
        return handlers(null, output, error);
    }

    public ExternalProcessBuilder handlers(InputHandler input, OutputHandler output) {
        return handlers(input, output, null);
    }

    public ExternalProcessBuilder handlers(InputHandler input, OutputHandler output, OutputHandler error) {
        if (error == null) {
            error = new StringOutputHandler();
        }

        return handler(new PluggableProcessHandler(input, output, error));
    }

    /**
     * Sets the maximum number of milliseconds the process is allowed to remain idle. The exact definition of idle may
     * vary slightly depending on the {@link OutputHandler} being used, but it generally correlates to time where the
     * running process is not producing any output to be processed. Concretely, the idle timeout defines the maximum
     * milliseconds between {@link Watchdog#resetWatchdog()} calls.
     *
     * @param idleTimeout the number of milliseconds the running process is allowed to be idle
     * @return {@code this}
     */
    public ExternalProcessBuilder idleTimeout(long idleTimeout) {
        settings.setIdleTimeout(idleTimeout);

        return this;
    }

    public ExternalProcessBuilder log(Logger logger, Priority priority) {
        return log(logger, priority, null);
    }

    public ExternalProcessBuilder log(Logger logger, Priority priority, StringObfuscator obfuscator) {
        return addMonitor(new LoggingProcessMonitor(logger, priority, obfuscator));
    }

    /**
     * @return {@code this}
     * @deprecated in 1.5.14 for removal in 2.0. This setting no longer has any effect.
     */
    @Deprecated
    public ExternalProcessBuilder suppressSpecialWindowsBehaviour() {
        return this;
    }

    /**
     * Sets the maximum number of milliseconds the process is allowed to remain idle. The exact definition of idle may
     * vary slightly depending on the {@link OutputHandler} being used, but it generally correlates to time where the
     * running process is not producing any output to be processed. Concretely, the idle timeout defines the maximum
     * milliseconds between {@link Watchdog#resetWatchdog()} calls.
     *
     * @param timeout the number of milliseconds the running process is allowed to be idle
     * @return {@code this}
     * @deprecated Use {@link #idleTimeout(long)} instead. This method will be removed in a future release.
     */
    @Deprecated
    public ExternalProcessBuilder timeout(long timeout) {
        return idleTimeout(timeout);
    }

    /**
     * Calls {@code .bat} files through {@code cmd.exe} and works around problems with arguments that contain quotes.
     * Due to the Windows implementation of Java's {@code Process} interface, arguments with quotes can be mishandled
     * and result in exceptions from the JDK internals.
     * <p>
     * Setting this has no effect if the command is not a {@code .bat} or {@code .cmd} script or if the process is
     * not being run on Windows.
     *
     * @return {@code this}
     */
    public ExternalProcessBuilder useQuotesInBatArgumentsWorkaround() {
        settings.setUseQuotesInBatArgumentsWorkaround(true);

        return this;
    }

    /**
     * @return {@code this}
     * @deprecated in 1.5.14 for removal in 2.0. This setting no longer has any effect.
     */
    @Deprecated
    public ExternalProcessBuilder useWindowsEncodingWorkaround() {
        return this;
    }
}
