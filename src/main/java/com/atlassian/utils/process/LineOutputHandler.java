package com.atlassian.utils.process;

import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;

/**
 * An Output Handler which breaks the output from the process into discrete lines and processes each line in turn.
 * Subclasses provide the appropriate implementation of the per-line processing method.
 */
public abstract class LineOutputHandler extends BaseOutputHandler {

    private final String encoding;

    protected LineOutputHandler() {
        this(null); // if not encoding is defined, use the default encoding from the admin
    }

    protected LineOutputHandler(String encoding) {
        this.encoding = encoding;
    }

    public String getEncoding() {
        return encoding;
    }
    
    public void process(InputStream output) throws ProcessException {
        LineReadable reader = createReader(output, encoding);

        process(reader);
    }

    /**
     * Allows derived classes to control the creation of the {@link LineReadable} which will be used to read lines
     * from the provided {@code InputStream}.
     * <p>
     * This default implementation uses a standard Java {@code BufferedReader} wrapping an {@code InputStreamReader}
     * to read the stream, wrapping that combination in a {@link BufferReaderLineReader}.
     * 
     * @param stream   the input stream to read from
     * @param encoding the encoding to use, which may be {@code null} for the default
     * @return a {@link BufferReaderLineReader}
     * @throws ProcessException Thrown if the specified encoding is not available
     * @since 1.5
     */
    protected LineReadable createReader(InputStream stream, String encoding) throws ProcessException {
        InputStreamReader reader;
        if (encoding == null) {
            reader = new InputStreamReader(stream);
        } else {
            try {
                reader = new InputStreamReader(stream, encoding);
            } catch (UnsupportedEncodingException e) {
                throw new ProcessException("Requested encoding [" + encoding + "] is not supported", e);
            }
        }
        
        return new BufferReaderLineReader(new BufferedReader(reader));
    }

    protected void process(LineReadable reader) throws ProcessException {
        int counter = 0;
        try {
            String line;
            while (!isCanceled() && (line = reader.readLine()) != null) {
                resetWatchdog();
                processLine(counter++, line);
            }
            processInputEnd(counter);
        } catch (InterruptedIOException e) {
            // This means the process was asked to stop which can be normal so we just finish
            processEndByException(counter);
        } catch (IOException e) {
            processEndByException(counter);
            throw new ProcessException(e);
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }

    protected void processEndByException(int counter) {
        // do nothing by default
    }

    /**
     * Input has finished
     *
     * @param lineCount total number of lines processed
     * @throws ProcessException To be thrown if the derived handler encounters an error processing the end of the input.
     */
    protected void processInputEnd(int lineCount) throws ProcessException {
        // do nothing by default
    }

    /**
     * Process the given line
     *
     * @param lineNum The line number of the line being processed
     * @param line the content of the line
     */
    protected abstract void processLine(int lineNum, String line);
    
    protected static class BufferReaderLineReader implements LineReadable {

        private final BufferedReader reader;

        BufferReaderLineReader(BufferedReader reader) {
            this.reader = reader;
        } 

        public String readLine() throws IOException {
            return reader.readLine();
        }

        public void close() throws IOException {
            reader.close();
        }
    }
}
