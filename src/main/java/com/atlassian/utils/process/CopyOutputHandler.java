package com.atlassian.utils.process;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InterruptedIOException;

/**
 * An Output Handler which copies the process output to a give output stream
 */
public class CopyOutputHandler extends BaseOutputHandler {
    
    private final OutputStream dest;
    private final int bufferSize;

    /**
     * Create a CopyOutputHandler to redirect output from the process to the given stream using the default buffer size
     * of 1024 bytes
     *
     * @param dest the stream to which output is to be written
     */
    public CopyOutputHandler(OutputStream dest) {
        this(dest, 4096);
    }

    /**
     * Create a CopyOutputHandler to redirect output from the process to the given stream using the specified
     * buffer size
     *
     * @param dest the stream to which output is to be written
     * @param bufferSize the buffer size to use for redirecting the output
     */
    public CopyOutputHandler(OutputStream dest, int bufferSize) {
        this.dest = dest;
        this.bufferSize = bufferSize;
    }

    public void process(InputStream output) throws ProcessException {
        try {
            byte buffer[] = new byte[bufferSize];
            int num;
            while ((num = output.read(buffer)) != -1) {
                resetWatchdog();
                dest.write(buffer, 0, num);
            }
        } catch (InterruptedIOException e) {
            // This means the process was asked to stop which can be normal so we just finish
        } catch (IOException e) {
            throw new ProcessException(e);
        }
    }
}
