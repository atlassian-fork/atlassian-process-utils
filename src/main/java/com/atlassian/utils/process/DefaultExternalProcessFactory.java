package com.atlassian.utils.process;

import org.apache.log4j.Logger;

import java.util.concurrent.*;

import javax.annotation.Nonnull;

/**
 * A default implementation of the {@link ExternalProcessFactory} which builds and configures instances of
 * {@link ExternalProcessImpl}.
 */
public class DefaultExternalProcessFactory implements ExternalProcessFactory {

    private static final Logger log = Logger.getLogger(DefaultExternalProcessFactory.class);

    private final ExecutorService pool;

    private boolean shutdown;

    public DefaultExternalProcessFactory() {
        this(createDefaultExecutorService());
    }

    public DefaultExternalProcessFactory(ExecutorService executorService) {
        this.pool = executorService;
    }

    private static ExecutorService createDefaultExecutorService() {
        final String pooledThreadName = "process-utils:idle";

        ThreadFactory threadFactory = r -> new Thread(r, pooledThreadName);

        return new ThreadPoolExecutor(6, Integer.MAX_VALUE, 2, TimeUnit.MINUTES,
                new SynchronousQueue<>(), threadFactory) {

            @Override
            protected void beforeExecute(Thread thread, Runnable runnable) {
                thread.setName(thread.getId() + ":" + ((LatchedRunnable) runnable).getName());
                super.beforeExecute(thread, runnable);
            }

            @Override
            protected void afterExecute(Runnable runnable, Throwable throwable) {
                Thread.currentThread().setName(pooledThreadName);
                super.afterExecute(runnable, throwable);
            }
        };
    }

    @Nonnull
    @Override
    public ExternalProcess create(@Nonnull ExternalProcessSettings settings) {
        if (shutdown) {
            throw new IllegalStateException("The DefaultExternalProcessFactory has been shutdown; new external processes cannot be created");
        }
        settings.validate();

        ExternalProcessImpl process =  new ExternalProcessImpl(pool, settings.getCommand(), settings.getProcessHandler());
        configureProcess(process, settings);

        return process;
    }

    /**
     * Attempts to shutdown the internal {@code ThreadPoolExecutor} which manages the I/O pumps for external processes.
     * To prevent memory leaks, web applications which use process-utils should always call this when they terminate.
     * <p>
     * On termination, an attempt is made to shutdown the thread pool gracefully. If that does not complete within
     * five seconds, shutdown is forced. That is given another five seconds to complete before the thread pool is
     * abandoned.
     *
     * @since 1.5
     */
    @Override
    public void shutdown() {
        shutdown = true;

        if (pool == null) {
            return;
        }
        log.debug("Attempting to shutdown pump executor service");

        pool.shutdown();
        try {
            if (pool.awaitTermination(5, TimeUnit.SECONDS)) {
                log.debug("Pump executor service has shutdown gracefully");
            } else {
                //The executor did not shutdown within the timeout. We can't wait forever, though, so issue a
                //shutdownNow() and give it another second
                log.warn("Pump executor service did not shutdown within the timeout; forcing shutdown");

                pool.shutdownNow();
                if (pool.awaitTermination(5, TimeUnit.SECONDS)) {
                    //The forced shutdown has brought the executor down. Not ideal, but acceptable
                    log.debug("Pump executor service has been forced to shutdown");
                } else {
                    //We can't delay execution indefinitely waiting, so log a warning. The JVM may not shut down
                    //if this service does not stop (because it uses non-daemon threads), so this may be helpful
                    //in debugging should that happen.
                    log.warn("Pump executor service did not shutdown; it will be abandoned");
                }
            }
        } catch (InterruptedException e) {
            log.warn("Interrupted while waiting for the pump executor service to shutdown; some worker threads may " +
                    "still be running");
        }
    }

    protected void configureProcess(@Nonnull ExternalProcessImpl process, @Nonnull ExternalProcessSettings settings) {
        process.setAsynchronous(settings.isAsynchronous());
        process.setEscapeInternalDoubleQuotesOnWindows(settings.isEscapeInternalDoubleQuotesOnWindows());
        process.setUseQuotesInBatArgumentsWorkaround(settings.isUseQuotesInBatArgumentsWorkaround());
        process.setWorkingDir(settings.getWorkingDirectory());

        for (ProcessMonitor monitor : settings.getMonitors()) {
            if (monitor == null) {
                throw new IllegalArgumentException("Null ProcessMonitor in the monitors collection");
            }
            process.addMonitor(monitor);
        }
        if (settings.hasEnvironment()) {
            process.setEnvironment(settings.getEnvironment());
        }
        if (settings.hasExecutionTimeout()) {
            process.setExecutionTimeout(settings.getExecutionTimeout());
        }
        if (settings.hasIdleTimeout()) {
            process.setIdleTimeout(settings.getIdleTimeout());
        }
    }
}
