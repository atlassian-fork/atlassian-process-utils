package com.atlassian.utils.process;

import java.io.InputStream;

/**
 * A Handler for process output
 *
 * The OutputHandler interface is designed to allow different output handling strategies to be plugged into
 * process handlers
 */
public interface OutputHandler {
    /**
     * Process an output stream generated by the external process (either stdout or stderr)
     *
     * @param output the external process' output stream (available as an input to this class)
     * @throws ProcessException if there is a problem processing the output
     */
    void process(InputStream output) throws ProcessException;

    /**
     * Called when the process completes. This call allows the output handler to close any
     * open resources and finalize any processing.
     *
     * @throws ProcessException if there is a problem completing processing
     */
    void complete() throws ProcessException;

    /**
     * Set the watchdog that this handler should be resetting to prevent the process from being terminated.
     * The watchdog should be called periodically based on output generated by the process.
     *
     * @param watchdog process watchdog instance.
     */
    void setWatchdog(Watchdog watchdog);
}
