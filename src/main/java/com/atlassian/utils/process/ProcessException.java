package com.atlassian.utils.process;

/**
 * Exceptions thrown while running external processes
 */
public class ProcessException extends Exception {
    int exitCode;

    public ProcessException(String message, Throwable cause) {
        super(message, cause);
        exitCode = extractExitCode(cause);
    }

    private static int extractExitCode(Throwable cause) {
        // Trawl the whole chain looking for a process exception - copy it's exit code.
        while (cause != null) {
            if (cause instanceof ProcessException) {
                return ((ProcessException) cause).getExitCode();
            }
            cause = cause.getCause();
        }
        return 0;
    }

    public ProcessException(String message) {
        super(message);
    }

    public ProcessException(Throwable e) {
        super(e);
        exitCode = extractExitCode(e);
    }

    public ProcessException(String message, int exitCode) {
        super(message);
        this.exitCode = exitCode;
    }

    public int getExitCode() {
        return exitCode;
    }
}
