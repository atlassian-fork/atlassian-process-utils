package com.atlassian.utils.process;

/**
 * Interface describing a watchdog which is used to manage a running process.
 * <p>
 * The watchdog helps prevent runaway processes by implementing an internal timer which kills the running process if
 * it is left untouched too long.
 */
public interface Watchdog {

    /**
     * Cancels the running process.
     */
    void cancel();

    /**
     * Retrieves a flag indicating whether the process has been canceled.
     *
     * @return {@code true} if the process has been canceled; otherwise, {@code false}
     * @since 1.5
     */
    boolean isCanceled();

    /**
     * Resets the internal timer, allowing the process to continue running.
     */
    void resetWatchdog();
}
