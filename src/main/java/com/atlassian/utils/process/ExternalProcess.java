package com.atlassian.utils.process;

/**
 * Describes a command and the process which runs it. External processes can be run two ways:
 * <ul>
 *     <li>Synchronously, using {@link #execute()} or {@link #executeWhile(Runnable)}</li>
 *     <li>Asynchronously, by {@link #start() starting} and later {@link #finish() finishing}</li>
 * </ul>
 */
public interface ExternalProcess extends Watchdog {

    /**
     * Executes the command and waits for the process to complete.
     */
    void execute();

    /**
     * Executes the command, waiting for the process to complete, and. while it is running, invokes the provided
     * {@code Runnable}. Process completion is not checked until the provided {@code Runnable} returns.
     *
     * @param runnable a task to perform while the process is running.
     */
    void executeWhile(Runnable runnable);

    /**
     * Waits for the process to complete, blocking until the process completes or the current thread is interrupted.
     * If the thread is interrupted while finish method is blocked waiting for the process to complete, the process
     * is canceled and an exception is set in the configured {@link #getHandler() handler}.
     */
    void finish();

    /**
     * Waits <i>up to</i> the specified number of milliseconds for the process to complete.
     * If the thread is interrupted while finish method is blocked waiting for the process to complete, the process
     * is canceled and an exception is set in the configured {@link #getHandler() handler}.
     *
     * @param maxWait the maximum amount of time in milliseconds to wait for the process to finish
     * @return {@code true} if the process completed within the timeout; otherwise, {@code false}
     */
    boolean finish(int maxWait);

    /**
     * @return the command line (command and arguments) for the process
     */
    String getCommandLine();

    /**
     * Retrieves the {@link ProcessHandler} which handles the process's input, output and error streams and receives
     * the process's exit code as well as any exceptions thrown when the process completes.
     *
     * @return the {@link ProcessHandler} instance associated with this process execution
     */
    ProcessHandler getHandler();

    /**
     * @return the time process execution started, or {@code -1} if the process has not yet started
     */
    long getStartTime();

    /**
     * Checks if right now the process is timed out.
     * If checked after process has executed there may race condition
     * when process didn't timed out during execution
     * but it is now as some time has passed
     * @return if process times out
     */
    boolean isTimedOut();

    /**
     * @return {@code true} if the process is currently still running, otherwise, {@code false} if the process has not
     *         been started or has finished
     */
    boolean isAlive();

    /**
     * Starts the process and sets up I/O pump threads to forward process I/O to the {@link ProcessHandler} and its
     * {@link InputHandler} and {@link OutputHandler}s. It is expected, when using {@code start()}, that eventually
     * {@link #finish()} (or {@link #finish(int)}) will be called.
     * <p>
     * The primary use case for starting a process instead of {@link #execute() executing} it is to allow other the
     * calling code to perform other operations while the process is running. When using this style of asynchronous
     * execution, it is recommended to call {@link ExternalProcessBuilder#asynchronous()} on the builder prior to
     * {@link ExternalProcessBuilder#build() building} the process.
     */
    void start();
}
