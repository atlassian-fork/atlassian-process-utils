package com.atlassian.utils.process;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.StringWriter;

/**
 * An {@link OutputHandler} which captures the output stream as a string.
 */
public class StringOutputHandler extends BaseOutputHandler {
    private final String encoding;
    private final StringWriter writer;

    public StringOutputHandler() {
        this(null);
    }

    public StringOutputHandler(String encoding) {
        this.encoding = encoding;
        
        writer = new StringWriter();
    }

    @Override
    public void complete() {
        IOUtils.closeQuietly(writer);
    }

    public String getOutput() {
        return writer.toString();
    }

    public void process(InputStream output) throws ProcessException {
        InputStreamReader reader = null;
        try {
            if (encoding == null) {
                reader = new InputStreamReader(output);
            } else {
                reader = new InputStreamReader(output, encoding);
            }

            char buffer[] = new char[1024];
            int num;
            while ((num = reader.read(buffer)) != -1) {
                resetWatchdog();
                writer.write(buffer, 0, num);
            }
        } catch (InterruptedIOException e) {
            // This means the process was asked to stop which can be normal so we just finish
        } catch (IOException e) {
            throw new ProcessException(e);
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }
}
