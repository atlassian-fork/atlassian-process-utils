package com.atlassian.utils.process;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import java.util.Stack;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A {@code Runnable} base class which uses a {@code CountDownLatch} to allow waiting
 */
@SuppressWarnings("unused") //Unused methods must be retained until 2.0
public abstract class LatchedRunnable implements Runnable {

    private static final Logger log = Logger.getLogger(LatchedRunnable.class);

    private final CountDownLatch latch;
    private final Stack<?> ndcStack;
    private final AtomicReference<Thread> runner;

    /**
     * @deprecated in 1.6 for removal in 2.0. Set during construction, runnable names should not be modified after.
     *             To access the name after construction, use {@link #getName()}.
     */
    @Deprecated
    protected String name;

    private volatile boolean canceled;

    /**
     * @deprecated in 1.6 for removal in 2.0. Implementors should provide their own name for their LatchedRunnables,
     *             and should attempt to use usefully descriptive names.
     */
    @Deprecated
    protected LatchedRunnable() {
        this("LatchedRunnable");
    }

    /**
     * Constructs a new {@code LatchedRunnable} with the specified name.
     *
     * @param name the name for the runnable
     * @throws IllegalArgumentException if the provided {@code name} is blank or empty
     * @throws NullPointerException if the provided {@code name} is {@code null}
     */
    @SuppressWarnings("deprecation")
    protected LatchedRunnable(String name) {
        if (name == null) {
            throw new NullPointerException("name");
        }
        if (name.trim().isEmpty()) {
            throw new IllegalArgumentException("A name is required for each LatchedRunnable");
        }

        this.name = name;

        latch = new CountDownLatch(1);
        ndcStack = NDC.cloneStack();
        runner = new AtomicReference<Thread>();
    }

    /**
     * Waits <i>up to</i> the specified number of milliseconds for processing to complete, returning a flag indicating
     * whether processing completed ({@code true}) or not ({@code false}).
     *
     * @param millis the number of milliseconds to wait for the runnable to complete
     * @return {@code true} if the runnable has completed before the specified delay elapses; otherwise, {@code false}
     */
    public boolean await(long millis) {
        return await(millis, TimeUnit.MILLISECONDS);
    }

    /**
     * Waits <i>up to</i> the specified number of milliseconds for processing to complete, returning a flag indicating
     * whether processing completed ({@code true}) or not ({@code false}).
     *
     * @param value the wait period, which will be converted to milliseconds
     * @param unit  the unit the wait period is in, used to convert to milliseconds
     * @return {@code true} if the runnable has completed before the specified delay elapses; otherwise, {@code false}
     */
    public boolean await(long value, TimeUnit unit) {
        try {
            return latch.await(value, unit);
        } catch (InterruptedException e) {
            log.warn("Interrupted waiting for ExternalProcess pump to complete");
            Thread.currentThread().interrupt();

            return false;
        }
    }

    /**
     * Cancels this runnable and {@link #interrupt() interrupts}.
     */
    public void cancel() {
        canceled = true;

        // Generally pointless, since doTask() can do anything, but logically correct
        interrupt();
    }

    /**
     * Retrieves the name for this runnable.
     *
     * @return the runnable's name
     */
    @SuppressWarnings("deprecation")
    public String getName() {
        return name;
    }

    /**
     * If this runnable is {@link #run() running}, interrupts the thread on which it is running.
     */
    public void interrupt() {
        Thread runnerThread = runner.get();
        if (runnerThread != null) {
            runnerThread.interrupt();
        }
    }

    /**
     * @return {@code true} if this runnable was {@link #cancel() canceled}; otherwise, {@code false} if it is still
     *         running or completed normally
     * @deprecated in 1.6 for removal in 2.0. Use {@link #isCanceled()} instead.
     */
    @Deprecated
    public boolean isCancelled() {
        return canceled;
    }

    /**
     * @return {@code true} if this runnable was {@link #cancel() canceled}; otherwise, {@code false} if it is still
     *         running or completed normally
     */
    public boolean isCanceled() {
        return canceled;
    }

    /**
     * @return {@code true} if this runnable is still {@link #run() running}; otherwise, {@code false} if it is still
     *         running (even if it has been {@link #cancel() canceled} and has not yet stopped)
     */
    public boolean isRunning() {
        return latch.getCount() > 0;
    }

    /**
     * Captures the running {@code Thread} and invokes {@link #doTask()}, releasing the running thread and counting
     * down the latch when the task completes.
     */
    public void run() {
        try {
            NDC.inherit(ndcStack);
            runner.set(Thread.currentThread());
            doTask();
        } finally {
            runner.set(null);
            NDC.remove();
            latch.countDown();
        }
    }

    /**
     * Performs processing for this runnable. Implementors are <i>strongly encouraged</i> not to throw exceptions.
     */
    protected abstract void doTask();
}