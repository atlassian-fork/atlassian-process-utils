package com.atlassian.utils.process;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

/**
 * ProcessMonitor that logs the results to the provided {@link Logger} using the provided {@link Priority}. When
 * the external process does not succeed, the error messages are logged using the info priority.
 */
public class LoggingProcessMonitor implements ProcessMonitor {
    private Logger logger;
    private Priority priority;
    private StringObfuscator obfuscator;
    
    public LoggingProcessMonitor(Logger logger, Priority priority) {
        this.logger = logger;
        this.priority = priority;
    }

    public LoggingProcessMonitor(Logger logger, Priority priority, StringObfuscator obfuscator) {
        this(logger, priority);
        this.obfuscator = obfuscator;
    }
    
    protected String obfuscate(String value) {
        if (obfuscator != null) {
            return obfuscator.obfuscate(value);
        } else {
            return value;
        }
    }
    
    /**
     * @param process the external process.
     * @return the command line. 
     */
    protected String getCommandLine(ExternalProcess process) {
        return obfuscate(process.getCommandLine());
    }

    /**
     * Logs message to Log4j logger
     */
    public void onBeforeStart(ExternalProcess process) {
        if (this.logger != null && this.logger.isEnabledFor(this.priority)) {
            logger.log(this.priority, "Starting process: " + getCommandLine(process));
        }
    }

    /**
     * Logs message to Log4j logger
     */
    public void onAfterFinished(ExternalProcess process) {
        if (this.logger != null && this.logger.isEnabledFor(this.priority)) {
            StringBuilder message = new StringBuilder();
            String commandLine = getCommandLine(process);
            message.append("Finished process: ").append(commandLine);
            Long startTime = process.getStartTime();
            if (startTime != null) {
            	message.append(" took ").append(System.currentTimeMillis() - startTime.longValue()).append("ms");
            }
            logger.log(priority, message.toString());
            
            // log errors if present
            ProcessHandler handler = process.getHandler();
            if (handler != null && !handler.succeeded()) {
                logger.info(getErrorMessage(process));
            }
        }
    }
    
    public String getErrorMessage(ExternalProcess process) {
        ProcessHandler handler = process.getHandler();
        String commandLine = getCommandLine(process);
        StringBuilder message = new StringBuilder();

        if (handler.getException() != null) {
            message.append("Exception executing command \"")
                .append(commandLine).append("\" ")
                .append(handler.getException().getMessage()).append("\n")
                .append(handler.getException()).append("\n");
        }

        String reason = null;
        if (handler instanceof PluggableProcessHandler) {
            OutputHandler errorHandler = ((PluggableProcessHandler) handler).getErrorHandler();
            if (errorHandler instanceof StringOutputHandler) {
                StringOutputHandler errorStringHandler = (StringOutputHandler) errorHandler;
                if (errorStringHandler.getOutput() != null) {
                    reason = errorStringHandler.getOutput();
                }
            }
        }
        if (reason != null && reason.trim().length() > 0) {
            message.append("Error executing command \"").append(commandLine).append("\": ").append(reason);
        }
        return obfuscate(message.toString());
    }
}
