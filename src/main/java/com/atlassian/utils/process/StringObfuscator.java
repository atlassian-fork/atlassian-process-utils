package com.atlassian.utils.process;

/**
 * Interface for obfuscating (parts of) a string. Useful for removing sensitive data from a string (passwords) before logging.
 */
public interface StringObfuscator {
    String obfuscate(String string);
}
