package com.atlassian.utils.process;

import org.apache.commons.lang.SystemUtils;
import org.junit.Test;
import org.jvnet.winp.WinProcess;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

public class RecursiveKillTest {

    @Test(timeout = 30 * 1000)
    public void testSimpleRecursiveKill() throws InterruptedException {
        //we can only live on windows...
        assumeTrue(SystemUtils.IS_OS_WINDOWS);

        ExternalProcessBuilder processBuilder = new ExternalProcessBuilder()
                .command(Arrays.asList("java", "com.atlassian.utils.process.RecursiveApp", Integer.toString(0), Integer.toString(5), Integer.toString(8)))
                .env("CLASSPATH", System.getProperty("java.class.path"))
                .executionTimeout(10 * 10000L);

        List<Integer> pids = new ArrayList<>();
        List<Integer> levels = new ArrayList<>();
        List<Integer> ended = new ArrayList<>();
        CountDownLatch latchForStart = new CountDownLatch(6);

        StringProcessHandler processHandler = new StringProcessHandler();
        processHandler.setOutputHandler(new BaseOutputHandler() {

            private final StringWriter writer = new StringWriter();

            public void process(InputStream output) throws ProcessException {
                try (BufferedReader childOutputReader = new BufferedReader(new InputStreamReader(output))) {
                    String line;
                    while ((line = childOutputReader.readLine()) != null) {
                        if (line.startsWith(RecursiveApp.PIDPREFIX)) {
                            Integer pid = Integer.parseInt(line.replace(RecursiveApp.PIDPREFIX, ""));
                            pids.add(pid);
                        } else if (line.endsWith(RecursiveApp.LEVEL_SUFFIX)) {
                            Integer lvl = Integer.parseInt(line.replace(RecursiveApp.LEVEL_SUFFIX, ""));
                            levels.add(lvl);

                            latchForStart.countDown();
                        } else if (line.endsWith(RecursiveApp.LEVEL_END_SUFFIX)) {
                            Integer lvl = Integer.parseInt(line.replace(RecursiveApp.LEVEL_END_SUFFIX, ""));
                            ended.add(lvl);
                        }
                        writer.write(line);
                    }
                } catch (InterruptedIOException e) {
                    // This means the process was asked to stop which can be normal so we just finish
                } catch (IOException e) {
                    throw new ProcessException(e);
                }
            }
        });
        processBuilder.handler(processHandler);

        ExternalProcess proc = processBuilder.build();
        proc.start();

        long start = System.currentTimeMillis();
        latchForStart.await(25, TimeUnit.SECONDS);
        assertTrue("Process startup time exceeded", ((System.currentTimeMillis() - start) < 25000));
        assertEquals("6 processes should have been started", 6, levels.size());

        proc.cancel();
        Thread.sleep(500);

        int count = getRecursiveAppCount(pids);
        assertEquals("All processes should have been killed", 0, count);
        assertEquals("No processes should have completed normally", 0, ended.size());

        String output = processHandler.getOutput();
        assertFalse("Root process should not have completed", output.contains(RecursiveApp.getEndMessage(0)));
        assertFalse("Level 1 process should not have completed", output.contains(RecursiveApp.getEndMessage(1)));
        assertFalse("Level 2 process should not have completed", output.contains(RecursiveApp.getEndMessage(2)));
        assertFalse("Level 3 process should not have completed", output.contains(RecursiveApp.getEndMessage(3)));
        assertFalse("Level 4 process should not have completed", output.contains(RecursiveApp.getEndMessage(4)));
        assertFalse("Level 5 process should not have completed", output.contains(RecursiveApp.getEndMessage(5)));
    }

    private static int getRecursiveAppCount(List<Integer> pids) {
        int count = 0;
        for (WinProcess process : WinProcess.all()) {
            if (pids.contains(process.getPid())) {
                ++count;
            }
        }
        return count;
    }
}
