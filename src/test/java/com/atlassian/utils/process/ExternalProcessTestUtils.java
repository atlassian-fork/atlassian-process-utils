package com.atlassian.utils.process;

import org.apache.commons.lang.SystemUtils;

import java.io.OutputStream;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Utility methods for testing ExternalProcess.
 */
public class ExternalProcessTestUtils {
    /**
     * @param blockTime the time to block on input processing
     * @param timeUnit the time unit
     * @return an InputHandler that writes a single character and then blocks for a while
     */
    public static InputHandler createBlockingInputHandler(final long blockTime, final TimeUnit timeUnit) {
        return new BaseInputHandler() {
            public void process(OutputStream input) {
                try {
                    // this is for windows pause. Wait 1 second then press a key
                    input.write("k".getBytes());
                    input.flush();

                    // sleep for a long time, the process should finish before the inputHandler finishes
                    // this simulates an inputHandler that simply copies data from some other stream into
                    // the process inputstream. It is quite common for the process to finish while the
                    // inputHandler is blocked on IO.
                    Thread.sleep(timeUnit.toMillis(blockTime));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    /**
     * Using ping because the command itself is cross-platform (existence-wise) and it produces a steady, continuous
     * flow of output. That allows a StringOutputHandler to prevent the process from ever idling into a timeout so
     * we can test the overall execution timeout
     *
     * @param count the number of ping requests that need to be attempted.
     * @param timeoutSeconds the time to wait between sending ping requests.
     * @param target the hostname / ip address to ping
     * @return an ExternalProcessBuilder for the ping command
     */
    public static ExternalProcessBuilder createPingBuilder(int count, long timeoutSeconds, String target) {
        ExternalProcessBuilder builder = new ExternalProcessBuilder();
        String countString = Integer.toString(count);
        if (SystemUtils.IS_OS_WINDOWS) {
            //ping on Windows uses -n for the count and -w for the timeout in _milliseconds_
            builder.command(Arrays.asList("ping", "-n", countString, "-w", Long.toString(timeoutSeconds * 1000), target));
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            //ping on MacOS uses -c and -W like Linux/Unix, but -W accepts a timeout in _milliseconds_
            builder.command(Arrays.asList("ping", "-c", countString, "-W", Long.toString(timeoutSeconds * 1000), target));
        } else {
            //ping on Linux (at least Ubuntu) uses -W for the timeout in _seconds_
            builder.command(Arrays.asList("ping", "-c", countString, "-W", Long.toString(timeoutSeconds), target));
        }
        return builder;
    }

    public static ExternalProcessBuilder createProcessBuilderForExecutionTimeoutTests(int durationSeconds) {
        // ensure the process executes a longer than the execution timeout
        return createProcessBuilderThatExecutesFor(durationSeconds + 10)
                .executionTimeout(TimeUnit.SECONDS.toMillis(durationSeconds))
                .idleTimeout(TimeUnit.SECONDS.toMillis(durationSeconds) + 250);
    }

    /**
     * @return a ExternalProcessBuilder for a command that will run for {@code duration}
     */
    public static ExternalProcessBuilder createProcessBuilderThatExecutesFor(int durationSeconds) {
        // 1 ping per second; we need n+1 pings to execute for n seconds
        int pingCount = durationSeconds + 1;
        return createPingBuilder(pingCount, durationSeconds, "localhost");
    }

    /**
     * @return an ExternalProcessBuilder with that will wait for {@code duration} without providing any output
     */
    public static ExternalProcessBuilder createProcessBuilderThatIdlesFor(long durationSeconds) {
        // ping to a non-existent ip address to force it to idle processing
        // note that the exit code will be 2, so the command will be marked as failed.
        return createPingBuilder(1, durationSeconds, "123.45.67.89");
    }

    public static ExternalProcessBuilder createProcessBuilderForIdleTimeoutTests() {
        return createProcessBuilderThatIdlesFor(2)
                .idleTimeout(500L)
                .executionTimeout(2000L);
    }
}
