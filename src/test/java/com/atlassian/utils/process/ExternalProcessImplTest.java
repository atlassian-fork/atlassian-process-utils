package com.atlassian.utils.process;

import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.*;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Collections;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.utils.process.ExternalProcessTestUtils.createBlockingInputHandler;
import static com.atlassian.utils.process.ExternalProcessTestUtils.createPingBuilder;
import static com.atlassian.utils.process.ExternalProcessTestUtils.createProcessBuilderForExecutionTimeoutTests;
import static com.atlassian.utils.process.ExternalProcessTestUtils.createProcessBuilderForIdleTimeoutTests;
import static com.atlassian.utils.process.ExternalProcessTestUtils.createProcessBuilderThatExecutesFor;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
public class ExternalProcessImplTest {

    @Test(timeout = 10 * 1000)
    public void testAsynchronous() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

        //Runs a single ping. This should complete nearly instantly. However, since ExternalProcessImpl.finish() is
        //not invoked until after the latch has already timed out, the only way for this test to succeed is if the
        //ExternalProcessImpl internals automatically wrap up (somehow)
        ExternalProcess process = createPingBuilder(1, 1, "localhost")
                .asynchronous() //Hint to ExternalProcess that this command will be executed asynchronously
                .handlers(new BaseOutputHandler() {

                    @Override
                    public void complete() {
                        //When ExternalProcessImpl.wrapUpProcess is called the ProcessHandler will complete all of
                        //the handlers. If the process doesn't wrap up the latch isn't released and the test fails
                        latch.countDown();
                    }

                    @Override
                    public void process(InputStream output) {
                        //We don't care about the output; we care that the process eventually stops
                    }
                })
                .build();
        process.start();

        try {
            assertTrue("The asynchronous process was not wrapped up", latch.await(2, TimeUnit.SECONDS));
        } finally {
            process.finish();
        }
    }

    @Test(timeout = 10 * 1000) //Test timeout in case execution doesn't timeout like it should
    public void testExecutionTimeout() {
        ExternalProcess process = createProcessBuilderForExecutionTimeoutTests(1)
                .handlers(new StringOutputHandler()) //Will ensure we don't idle out
                .build();
        process.execute();

        ProcessException exception = process.getHandler().getException();
        assertTrue(exception instanceof ProcessTimeoutException);
    }

    @Test(timeout = 10 * 1000) //Test timeout in case execution doesn't stop timeout like it should
    public void testIdleTimeout() {
        //Ensure the timeout here is less than the timeout on the test
        ExternalProcess process = createProcessBuilderForIdleTimeoutTests()
                .handlers(new StringOutputHandler())
                .build();
        process.execute();

        ProcessException exception = process.getHandler().getException();
        assertTrue(exception instanceof ProcessTimeoutException);
    }

    @Test(timeout = 10 * 1000) // Test timeout in case execution doesn't stop as it should
    public void testBlockingInputHandlerAbortsWhenProcessFinishes() {
        ExternalProcess process = buildProcessWithBlockingInputHandler();

        process.start();
        process.finish();
        
        // the process should finish by itself and should not be marked as canceled.
        assertFalse(process.isCanceled());
        assertTrue(process.getHandler().succeeded());
        assertNull(process.getHandler().getException());
        assertEquals(0, process.getHandler().getExitCode());
    }

    @Test(timeout = 10 * 1000) // Test timeout in case execution doesn't stop as it should
    public void testBlockingInputHandlerAbortsWhenProcessFinishes2() {
        ExternalProcess process = buildProcessWithBlockingInputHandler();

        // the test succeeds if it does not time out.
        process.start();

        // the process should finish by itself and should not be marked as canceled.
        assertTrue(process.finish(10 * 1000));
        
        assertFalse(process.isCanceled());
        assertTrue(process.getHandler().succeeded());
        assertNull(process.getHandler().getException());
        assertEquals(0, process.getHandler().getExitCode());
    }

    @Test(timeout = 10 * 1000)
    public void testNativeWindowsKilling() {
        assumeTrue(SystemUtils.IS_OS_WINDOWS);

        ExternalProcess process = new ExternalProcessBuilder()
                .command(Collections.singletonList("pause"))
                .handlers(new StringOutputHandler())
                .build();
        process.start();
        process.cancel();
    }

    /**
     * Not sure whether this code path makes a whole lot of sense, but there's quite a bit of code for dealing with
     * this condition, so better to test that it works..
     */
    @Test(timeout = 10 * 1000)
    public void testFinishWithoutStart() {
        ExternalProcess process = buildProcessWithBlockingInputHandler();

        process.finish();
    }

    @Test(timeout = 10 * 1000)
    public void testFinishWithTimeoutWithoutStart() {
        ExternalProcess process = buildProcessWithBlockingInputHandler();

        assertTrue(process.finish(100));
    }

    @Test(expected = IllegalStateException.class)
    public void testExecuteTwice() {
        StringOutputHandler handler = new StringOutputHandler();

        ExternalProcess process = createProcessBuilderThatExecutesFor(1)
                .handlers(handler)
                .build();

        process.execute();

        //Second execution should trigger an IllegalStateException
        process.execute();
    }

    @Test
    public void testPasswordsInEnvironmentAreNotLogged() {
        Logger processLogger = Logger.getLogger(ExternalProcessImpl.class);
        Level level = processLogger.getEffectiveLevel();
        StringWriter writer = new StringWriter();
        Appender appender = new WriterAppender(new SimpleLayout(), writer);
        try {
            processLogger.setLevel(Level.DEBUG);
            processLogger.addAppender(appender);

            ExternalProcess process = createProcessBuilderThatExecutesFor(1)
                    .handlers(createBlockingInputHandler(3, TimeUnit.SECONDS), new StringOutputHandler())
                    .env("TOP_SECRET_PASSWORD", "should not be logged")
                    .build();
            process.execute();

            String log = writer.toString();
            assertTrue("log wasn't captured", log.contains("TOP_SECRET_PASSWORD"));
            assertFalse("password was captured", log.contains("should not be logged"));
        } finally {
            processLogger.setLevel(level);
            processLogger.removeAppender(appender);
        }
    }
    
    @Test
    public void testProcessCreationFailureHandling() {
        String command = SystemUtils.IS_OS_WINDOWS ?
                "C:\\some\\nonexistent\\path\\bogus.exe" :
                "/some/nonexistent/path/bogus";

        ProcessHandler handler = mock(ProcessHandler.class);
        ExternalProcess process = new ExternalProcessBuilder()
                .command(Collections.singletonList(command))
                .handler(handler)
                .build();
        process.start();
        process.finish();

        ArgumentCaptor<ProcessException> exceptionCaptor = ArgumentCaptor.forClass(ProcessException.class);

        //Verify that ProcessHandler.complete was invoked, to ensure the exception caught in start() isn't swallowed
        verify(handler).complete(eq(-1), eq(false), exceptionCaptor.capture());
        verifyNoMoreInteractions(handler);

        //Verify that an exception was provided
        ProcessException exception = exceptionCaptor.getValue();
        assertTrue(exception instanceof ProcessNotStartedException);
        assertEquals(command + " could not be started", exception.getMessage());
    }

    @Test
    public void testCancelledProcessIsNotSuccessful() throws IOException, InterruptedException {
        final AtomicReference<ExternalProcess> processReference = new AtomicReference<ExternalProcess>();

        final ExternalProcess process = createProcessBuilderThatExecutesFor(10)
                .handlers(createBlockingInputHandler(10, TimeUnit.SECONDS), new BaseOutputHandler() {
                    @Override
                    public void process(InputStream output) throws ProcessException {
                        processReference.get().cancel();
                    }
                }).build();

        processReference.set(process);

        process.execute();

        assertTrue("A cancelled process is not cancelled", process.getHandler().isCanceled() && process.isCanceled());
        assertFalse("A cancelled process is successful", process.getHandler().succeeded());
        assertNull("No exception was expected", process.getHandler().getException());
    }

    @Test(timeout = 10 * 1000L)
    public void testThreadpoolShutdownDetectedAsProcessCancellation() throws InterruptedException {
        final ExecutorService threadPool = Executors.newCachedThreadPool();
        final CountDownLatch startedLatch = new CountDownLatch(1);
        final CountDownLatch blockForever = new CountDownLatch(1);

        ExternalProcessFactory original = ExternalProcessBuilder.getExternalProcessFactory();
        try {
            // set a custom factory that uses the threadpool
            ExternalProcessBuilder.setExternalProcessFactory(new DefaultExternalProcessFactory(threadPool));
            ExternalProcess process = createProcessBuilderForExecutionTimeoutTests(10)
                    .handlers(new BaseOutputHandler() {
                        @Override
                        public void process(InputStream output) throws ProcessException {
                            try {
                                startedLatch.countDown();
                                blockForever.await();
                            } catch (InterruptedException e) {
                                Thread.currentThread().interrupt(); // restore interrupted flag
                                throw new ProcessException(e);
                            }
                        }
                    }).build();

            process.start();
            // wait for the pump threads to start
            startedLatch.await(2, TimeUnit.SECONDS);

            // shutdown the threadpool, interrupting all pump threads
            threadPool.shutdownNow();

            // wait for the process to finish
            process.finish();

            assertTrue("process that was terminated early must be marked as canceled", process.isCanceled());
            assertFailedWithProcessException(process.getHandler());
        } finally {
            // restore the factory
            ExternalProcessBuilder.setExternalProcessFactory(original);
        }
    }

    @Test(timeout = 10 * 1000L)
    public void testInterruptedFinishMarkedCancelled() throws InterruptedException {
        final CountDownLatch startedLatch = new CountDownLatch(1);
        final CountDownLatch blockForever = new CountDownLatch(1);

        final ExternalProcess process = createProcessBuilderForExecutionTimeoutTests(10)
                .handlers(new BaseOutputHandler() {
                    @Override
                    public void process(InputStream output) throws ProcessException {
                        try {
                            startedLatch.countDown();
                            blockForever.await();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt(); // restore interrupted flag
                            throw new ProcessException(e);
                        }
                    }
                }).build();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                process.execute();
            }
        });

        // start process on a separate thread and wait for the pump threads to start
        thread.start();
        startedLatch.await(2, TimeUnit.SECONDS);

        // interrupt thread and wait for the process to terminate
        thread.interrupt();
        thread.join();

        assertTrue("process that was terminated early must be marked as canceled", process.isCanceled());
        assertFailedWithProcessException(process.getHandler());
    }

    private static ExternalProcess buildProcessWithBlockingInputHandler() {
        return createProcessBuilderThatExecutesFor(1)
                .handlers(createBlockingInputHandler(10, TimeUnit.SECONDS), new StringOutputHandler())
                .build();
    }

    private void assertFailedWithProcessException(ProcessHandler handler) {
        assertFalse("Process succeeded but failed was expected", handler.succeeded());
        assertNotNull("A ProcessException was expected", handler.getException());
    }
}
