package com.atlassian.utils.process;

import org.jvnet.winp.WinProcess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RecursiveApp {

    public static final String PIDPREFIX = "PID: ";
    public static final String LEVEL_SUFFIX = " started";
    public static final String LEVEL_END_SUFFIX = " ended";

    public static String getStartMessage(int level) {
        return level + LEVEL_SUFFIX;
    }

    public static String getEndMessage(int level) {
        return level + LEVEL_END_SUFFIX;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        int currentLevel = Integer.parseInt(args[0]);
        int maxLevel = Integer.parseInt(args[1]);
        int timeOut = Integer.parseInt(args[2]);

        System.out.println(getStartMessage(currentLevel));

        if (currentLevel < maxLevel) {
            //recurse

            System.out.println(currentLevel + " recursing");
            String[] command = new String[]{"java", "com.atlassian.utils.process.RecursiveApp", Integer.toString(currentLevel + 1), Integer.toString(maxLevel), Integer.toString(timeOut)};
            for (String s : command) {
                System.out.print(s);
                System.out.print(" ");
            }
            System.out.println();
            ProcessBuilder builder = new ProcessBuilder().command(command);
            builder.environment().put("CLASSPATH", System.getProperty("java.class.path"));
            Process child = builder.start();

            WinProcess childP = new WinProcess(child);
            int pid = childP.getPid();
            System.out.println(PIDPREFIX+pid);


            BufferedReader childOutputReader = new BufferedReader(
                    new InputStreamReader(child.getInputStream()));
            String line;


            while ((line = childOutputReader.readLine()) != null) {
                System.out.println(line);
            }

            child.waitFor();
        } else {
            long end = System.currentTimeMillis() + (timeOut * 1000);
            while (System.currentTimeMillis() < end) {
                try {
                    Thread.sleep(timeOut);
                } catch (InterruptedException ignored) {
                }
            }
        }
        System.out.println(getEndMessage(currentLevel));
    }
}
